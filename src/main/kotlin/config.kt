import kotlinx.serialization.*

@Serializable
class Config(
    val delay: Int,
    val dispersion: Double,
    val text: String,
    val to: String,
    val initialDelay: Long,
    val from: String
)
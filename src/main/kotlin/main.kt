import androidx.compose.desktop.Window
import androidx.compose.desktop.WindowEvents
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.charleskorn.kaml.Yaml
import kotlinx.coroutines.*
import java.io.File
import kotlin.random.Random

fun main() {
    runBlocking {
        val config: Config =
            try {
                println(File(".").absolutePath)
                val configRaw = File("config.yaml").readText()
                Yaml.default.decodeFromString(Config.serializer(), configRaw)
            } catch (e: Exception) {
                Config(300, 0.1, "txt", "receiver", 0, "Sender")
            }

        val message = config.text.replace("\\\\n\\s*".toRegex(), "\n")
        var index = 0
        val text = mutableStateOf("")
        var running = true

        val start = CompletableDeferred<Unit>()
        launch {
            start.await()
            delay(config.initialDelay)
            while (running) {
                randomDelay(config.delay, config.dispersion)
                text.value += message[index++]
                running = index < message.length
            }
        }

        Window(events = WindowEvents(onOpen = { start.complete(Unit) }),
            title = "Mail"
        ) {
            MaterialTheme(
                colors = darkColors(
                    surface = Color.DarkGray,
                    onSurface = Color.White
                ),
                typography = Typography(
                    body1 = TextStyle(
                        fontWeight = FontWeight.Normal,
                        fontSize = 24.sp,
                        letterSpacing = 0.5.sp
                    )
                )
            ) {
                Surface(modifier = Modifier.fillMaxSize()) {
                    Column {
                        Card(
                            modifier = Modifier.fillMaxWidth(0.95f).padding(10.dp),
                            elevation = 5.dp
                        ) {
                            Row(modifier = Modifier.padding(10.dp)) {
                                Column {
                                    Text("ОТ: ", fontWeight = FontWeight.Bold)
                                    Text("КОМУ: ", fontWeight = FontWeight.Bold)
                                }
                                Column {
                                    Text(config.from)
                                    Text(config.to)
                                }
                            }
                        }
                        Text(text.value, modifier = Modifier.padding(10.dp))
                    }
                }
            }
        }
    }
}

suspend fun randomDelay(baseDelay: Int, dispersion: Double) {
    val delayVal = baseDelay + Random.nextLong(
        (baseDelay * (1 - dispersion)).toLong(),
        (baseDelay * (1 + dispersion)).toLong()
    )
    delay(delayVal)
}
